const fs = require("fs");
const path = require("path");
const express = require("express");
const compression = require("compression");
const cors = require("cors");
const { FlightRadar24API } = require("flightradarapi");
const { CYMX_BOUNDS, DATA_FILE, CACHE_DURATION } = require("./constants");

const app = express();
const port = process.env.PORT || 3001;

const fetchData = async () => {
  const frApi = new FlightRadar24API();
  const bounds = frApi.getBoundsByPoint(...CYMX_BOUNDS);
  const data = await frApi.getFlights(null, bounds);
  return data;
};
const shouldUpdateCache = async () => {
  return new Promise((resolve) => {
    fs.stat(DATA_FILE, (err, stats) => {
      if (err) {
        resolve(true);
        return;
      }
      resolve((Date.now() - stats.mtime.getTime()) / 1000 > CACHE_DURATION);
    });
  });
};
const updateCache = (data) =>
  fs.writeFile(DATA_FILE, JSON.stringify(data), (err) => {
    if (err) {
      console.log(err);
    }
  });
const getDataFromCache = (res) =>
  fs.readFile(DATA_FILE, (err, data) => {
    console.log(err, data);
    res.json(JSON.parse(data));
  });

app.use(cors());
app.use(compression());

app.get("/", async (req, res) => {
  if (await shouldUpdateCache()) {
    const data = await fetchData();
    updateCache(data);
    res.json(data);
  } else {
    getDataFromCache(res);
  }
});

app.get("/health", (req, res) => {
  res.send("OK");
});

app.use("/assets", express.static(path.join(__dirname, "assets")));

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
