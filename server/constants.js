const path = require("path");

module.exports = {
  CYMX_BOUNDS: [45.6700, -74.0323, 20000],
  DATA_FILE: path.join(__dirname, "./data/data.json"),
  CACHE_DURATION: 1,
};
