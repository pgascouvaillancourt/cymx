const path = require("path");

module.exports = {
  apps: [
    {
      name: "cymx-api",
      script: path.join(__dirname, "./server/index.js"),
      env: {
        NODE_ENV: "production",
      },
    },
  ],
};
