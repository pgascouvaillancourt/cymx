import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import path from "path";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  build: {
    outDir: "./server/assets/",
    lib: {
      entry: path.resolve(__dirname, "src/main.js"),
      name: "CYMX",
      fileName: (format) => `cymx.${format}.js`,
    },
  },
});
