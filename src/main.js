import { useResizeObserver } from "@vueuse/core";
import { useFlights } from "./utils/useFlights";
import {
  PLANE_SIZE,
  TRAIL_SIZE,
  FLIGHT_DEPARTING_COLOR,
  FLIGHT_ARRIVING_COLOR,
  FLIGHT_PASSING_BY_COLOR,
} from "./constants";

const { flights, resetFlights } = useFlights();

const draw = () => {
  if (flights.length) {
    // Clear planes canvas
    [...document.querySelectorAll(".plane-canvas")].forEach((canvas) => {
      const ctx = canvas.getContext("2d");
      ctx.clearRect(0, 0, canvas.width, canvas.height);
    });

    // Draw each flight
    for (let flight of flights) {
      const { previousPosition, currentPosition } = flight;

      flight.planeCtx.fillStyle = flight.trajectory.color;
      flight.trailCtx.strokeStyle = flight.trajectory.color;

      // Draw planes
      flight.planeCtx.drawImage(
        window[flight.trajectory.color],
        currentPosition.x,
        currentPosition.y
      );

      // Write flight data
      flight.planeCtx.drawImage(
        flight.textCanvas,
        currentPosition.x,
        currentPosition.y
      );

      // Draw trails
      if (previousPosition) {
        flight.trailCtx.beginPath();
        flight.trailCtx.moveTo(
          previousPosition.x + (PLANE_SIZE - TRAIL_SIZE) / 2,
          previousPosition.y + (PLANE_SIZE - TRAIL_SIZE) / 2
        );
        flight.trailCtx.lineTo(
          currentPosition.x + (PLANE_SIZE - TRAIL_SIZE) / 2,
          currentPosition.y + (PLANE_SIZE - TRAIL_SIZE) / 2
        );
        flight.trailCtx.stroke();
      }
      flight.trajectory.steps += 1;
    }
  }

  window.requestAnimationFrame(draw);
};

document.addEventListener("DOMContentLoaded", () => {
  [
    [FLIGHT_DEPARTING_COLOR, FLIGHT_DEPARTING_COLOR],
    [FLIGHT_ARRIVING_COLOR, FLIGHT_ARRIVING_COLOR],
    [FLIGHT_PASSING_BY_COLOR, FLIGHT_PASSING_BY_COLOR],
  ].forEach(([canvasName, fillColor]) => {
    const preRenderedCanvas = document.createElement("canvas");
    preRenderedCanvas.width = PLANE_SIZE;
    preRenderedCanvas.height = PLANE_SIZE;

    const preRenderedCtx = preRenderedCanvas.getContext("2d");
    preRenderedCtx.fillStyle = fillColor;
    preRenderedCtx.fillRect(0, 0, PLANE_SIZE, PLANE_SIZE);
    window[canvasName] = preRenderedCanvas;
  });

  useResizeObserver(document.body, resetFlights);

  draw();
});
