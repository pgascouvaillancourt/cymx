import { getFormattedFlightsData } from "./utils/flights";
import { getControlPoint, getStartAndEndPoints } from "./utils/math";
import {
  API_URL,
  NEW_FLIGHT_INTERVAL,
  WORKER_MESSAGE_INIT,
  WORKER_MESSAGE_FLIGHT_STARTED,
} from "./constants";
// import data from "./data.json";

let width, height;

const movingFlights = [];
const startedFlights = [];

const fetchFlights = async () => {
  try {
    const response = await fetch(API_URL);
    const json = await response.json();
    const formattedFlightsData = getFormattedFlightsData(json);
    // const formattedFlightsData = getFormattedFlightsData(data);

    // Add moving flights to the list
    for (let flight of formattedFlightsData) {
      const isMoving = !flight.isOnGround && flight.altitude > 0;
      if (isMoving && !movingFlights.find(({ id }) => id === flight.id)) {
        movingFlights.push({ ...flight, started: false });
      }
    }

    // Start the first availble flight from the moving flights list
    const firstNonStartedFlightIndex = movingFlights.findIndex(
      ({ started }) => !started
    );
    if (firstNonStartedFlightIndex !== -1) {
      movingFlights[firstNonStartedFlightIndex].started = true;
      const flight = {
        ...movingFlights[firstNonStartedFlightIndex],
        controlPoint: getControlPoint(width, height),
      };
      const { startPoint, endPoint } = getStartAndEndPoints(
        flight.bearing,
        width,
        height
      );
      flight.startPoint = startPoint;
      flight.endPoint = endPoint;
      startedFlights.push(flight);
      postMessage([WORKER_MESSAGE_FLIGHT_STARTED, flight]);
    }
  } catch (e) {
    console.error(e);
  } finally {
    setTimeout(fetchFlights, NEW_FLIGHT_INTERVAL);
  }
};
fetchFlights();

onmessage = ({ data: [type, payload] }) => {
  if (type === WORKER_MESSAGE_INIT) {
    movingFlights.splice(0, movingFlights.length);
    startedFlights.splice(0, startedFlights.length);
    width = payload.width;
    height = payload.height;
  }
};
