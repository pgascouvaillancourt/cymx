const getRandomInt = (min, max) => Math.floor(Math.random() * max + min);

export const getQuadraticBezierXYatT = (startPt, controlPt, endPt, T) => {
  const tPow = Math.pow(T, 2);
  const invTPow = Math.pow(1 - T, 2);
  const c = 2 * (1 - T) * T;

  const x = invTPow * startPt.x + c * controlPt.x + tPow * endPt.x;
  const y = invTPow * startPt.y + c * controlPt.y + tPow * endPt.y;

  return {
    x,
    y,
  };
};

export const getControlPoint = (baseWidth, baseHeight) => {
  const xFactor = 3;
  const yFactor = 3;
  const areaWidth = baseWidth / xFactor;
  const areaHeight = baseHeight / yFactor;
  const x = (baseWidth - areaWidth) / 2;
  const y = (baseHeight - areaHeight) / 2;
  return {
    x: getRandomInt(x, x + areaWidth),
    y: getRandomInt(y, y + areaHeight),
  };
};

export const getStartAndEndPoints = (angle = 0, width, height) => {
  const startPoint = { x: 0, y: 0 };
  const endPoint = { x: 0, y: 0 };

  const startsRight = angle > 90 && angle < 270;
  const pxPerDeg = height / 180;

  let normalizedAngle;
  if (startsRight) {
    startPoint.x = width;
    normalizedAngle = angle - 90;
  } else {
    endPoint.x = width;
    normalizedAngle = angle <= 90 ? 90 - angle : 360 - angle + 90;
  }

  const dist = pxPerDeg * normalizedAngle;
  startPoint.y = height - dist;
  endPoint.y = dist;

  return { startPoint, endPoint };
};
