import { Flight } from "../Flight";
import {
  WORKER_MESSAGE_INIT,
  WORKER_MESSAGE_FLIGHT_STARTED,
  TRAIL_SIZE,
} from "../constants";
import Worker from "../worker?worker&inline";

const worker = new Worker();

export const useFlights = () => {
  const flights = [];
  let canvasMap = {};

  const getOrCreateCanvasAt = (scrollY) => {
    if (canvasMap[scrollY]) {
      return canvasMap[scrollY];
    }

    const { clientWidth } = document.body;
    const { innerHeight } = window;

    const planeCanvas = document.createElement("canvas");
    const trailCanvas = document.createElement("canvas");

    [
      ["plane", planeCanvas],
      ["trail", trailCanvas],
    ].forEach(([id, canvas]) => {
      canvas.width = clientWidth;
      canvas.height = innerHeight;
      canvas.id = `${id}-canvas-${scrollY}`;
      canvas.className = `cymx-canvas ${id}-canvas`;

      canvas.style.position = "absolute";
      canvas.style.top = `${scrollY}px`;
      canvas.style.left = 0;
      canvas.style.pointerEvents = "none";
      canvas.style.zIndex = 9999;
    });

    const planeCtx = planeCanvas.getContext("2d");
    const trailCtx = trailCanvas.getContext("2d");

    // Canvas settings
    trailCtx.lineWidth = TRAIL_SIZE;

    document.body.appendChild(planeCanvas);
    document.body.appendChild(trailCanvas);

    canvasMap[scrollY] = { planeCtx, trailCtx };

    return canvasMap[scrollY];
  };

  const initWorker = () => {
    const { clientWidth } = document.body;
    const { innerHeight } = window;
    worker.postMessage([
      WORKER_MESSAGE_INIT,
      { width: clientWidth, height: innerHeight },
    ]);
  };

  const resetFlights = () => {
    canvasMap = {};
    [...document.querySelectorAll(".cymx-canvas")].forEach((canvas) => {
      canvas.remove();
    });
    flights.splice(0, flights.length);
    initWorker();
  };

  worker.onmessage = ({ data: [type, payload] }) => {
    if (type === WORKER_MESSAGE_FLIGHT_STARTED) {
      flights.push(new Flight(payload, getOrCreateCanvasAt(window.scrollY)));
    }
  };

  return { flights, resetFlights };
};
