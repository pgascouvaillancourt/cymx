import {
  IATA,
  FLIGHT_DEPARTING_COLOR,
  FLIGHT_ARRIVING_COLOR,
  FLIGHT_PASSING_BY_COLOR,
} from "../constants";

export const getFormattedFlightsData = (data) => {
  const formattedFlightsData = [];

  for (let flight of data) {
    const {
      latitude: lat,
      longitude: lon,
      id,
      // icao24bit,
      heading: bearing,
      altitude,
      groundSpeed: speed,
      aircraftCode: model,
      registration,
      time: timestamp,
      originAirportIata: origin,
      destinationAirportIata: destination,
      // number,
      // airlineIata,
      onGround: isOnGround,
      verticalSpeed: rateOfClimb,
      callsign,
      // airlineIcao,
    } = flight;
    formattedFlightsData.push({
      id,
      lat,
      lon,
      bearing,
      altitude,
      speed,
      model,
      registration,
      timestamp,
      origin,
      destination,
      isOnGround: Boolean(isOnGround),
      rateOfClimb,
      callsign,
    });
  }
  return formattedFlightsData;
};

export const getFlightColor = ({ origin, destination }) => {
  if (origin === IATA) {
    return FLIGHT_DEPARTING_COLOR;
  }
  if (destination === IATA) {
    return FLIGHT_ARRIVING_COLOR;
  }
  return FLIGHT_PASSING_BY_COLOR;
};
