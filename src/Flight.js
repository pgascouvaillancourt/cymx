import { getQuadraticBezierXYatT } from "./utils/math";
import { getFlightColor } from "./utils/flights";
import { DURATION, PLANE_SIZE, FONT_SIZE, TEXT_MARGIN_TOP } from "./constants";

export class Flight {
  constructor(data, { planeCtx, trailCtx }) {
    // Store flight data
    this.data = data;

    // Store canvas references
    this.planeCtx = planeCtx;
    this.trailCtx = trailCtx;

    // Calculate trajectory
    const { startPoint, controlPoint, endPoint } = data;
    this.trajectory = {
      steps: 0,
      color: getFlightColor(this.data),
      startPoint,
      controlPoint,
      endPoint,
    };
    this.previousPosition = null;

    // Text canvas
    const textCanvas = document.createElement("canvas");
    textCanvas.width = 100;
    textCanvas.height = 100;
    const textCanvasCtx = textCanvas.getContext("2d");
    textCanvasCtx.fillStyle = this.trajectory.color;
    textCanvasCtx.font = `${FONT_SIZE}px sans-serif`;
    this.textCanvas = textCanvas;
    this.textCanvasCtx = textCanvasCtx;
    this.drawTextCanvas();
  }

  updateData(data) {
    this.data = data;
  }

  drawTextCanvas() {
    this.formattedFlightInfo.forEach((line, i) => {
      const textTop = PLANE_SIZE + (i + 1) * FONT_SIZE + TEXT_MARGIN_TOP;
      this.textCanvasCtx.fillText(line, 0, textTop);
    });
  }

  get currentPosition() {
    const position = getQuadraticBezierXYatT(
      this.trajectory.startPoint,
      this.trajectory.controlPoint,
      this.trajectory.endPoint,
      this.trajectory.steps / DURATION
    );
    this.previousPosition = position;
    return position;
  }

  /**
   * A flight is considered at destination when we have drawn all the steps of the trajectory.
   * We increase DURATION by an arbitrary percentage  to make sure flights aren't visible anymore
   * when we stop drawing them.
   */
  get isAtDestination() {
    return this.trajectory.steps > DURATION * 1.1;
  }

  get formattedFlightInfo() {
    const lines = [];
    if (this.aircraftModel) {
      lines.push(this.aircraftModel);
    }
    lines.push(`Latitude: ${this.latitude}`, `Longitude: ${this.longitude}`);
    if (this.altitude) {
      lines.push(`Altitude: ${this.altitude}ft`);
    }
    if (this.origin && this.destination) {
      lines.push(`${this.origin} to ${this.destination}`);
    }

    return lines.filter(Boolean);
  }

  get id() {
    return this.data.id;
  }

  get latitude() {
    return this.data.lat;
  }

  get longitude() {
    return this.data.lon;
  }

  get altitude() {
    return this.data.altitude;
  }

  get aircraftModel() {
    return this.data.model || "";
  }

  get origin() {
    return this.data.origin || "";
  }

  get destination() {
    return this.data.destination || "";
  }
}
