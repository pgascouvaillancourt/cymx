export const API_URL = "https://cymx.xn--prspect-b1a.art/";
export const IATA = "YMX";
export const NEW_FLIGHT_INTERVAL = 5000;

// Graphics
export const PLANE_SIZE = 10;
export const TRAIL_SIZE = 0.5;
export const DURATION = 3000;
export const FONT_SIZE = 10;
export const TEXT_MARGIN_TOP = 3;
export const FLIGHT_DEPARTING_COLOR = "#0f0";
export const FLIGHT_ARRIVING_COLOR = "magenta";
export const FLIGHT_PASSING_BY_COLOR = "cyan";

// Web worker messages
export const WORKER_MESSAGE_INIT = "WORKER_MESSAGE_INIT";
export const WORKER_MESSAGE_FLIGHT_STARTED = "WORKER_MESSAGE_FLIGHT_STARTED";
