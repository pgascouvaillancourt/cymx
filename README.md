# CYMX

## Setup

1. Install the dependencies:

   ```sh
   yarn
   ```

1. Run the server:

   ```sh
   node ./server
   ```

1. Run the app:

   ```sh
   yarn dev
   ```

## Building for production

To build the production assets, run `yarn build`. This produces two bundles:

* `cymx.es.js`
* `cymx.umd.js`

Include either in an HTML page to boot up the app.

## src/

`src/` contains the app's code.

## server/

`server/` contains the API code.
